package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.adaptor.OutboundAdaptor;
import th.co.ktb.next.archetype.interceptor.KafkaOutboundInterceptor;
import th.co.ktb.next.archetype.model.request.SystemRequestEntity;
import th.co.ktb.next.archetype.model.response.SystemResponseEntity;
import th.co.ktb.next.common.service.BaseService;

/*
* Service layer will contain business logic of the particular API and perform orchestration required.
* This layer implements BaseService interface.
*/
@Log4j2
@Service
public class PostMessageTopicBService implements BaseService<SystemRequestEntity, SystemResponseEntity> {

    @Value("${spring.cloud.stream.bindings.output-stream-2.destination}")
    private String topic;

    private OutboundAdaptor outboundAdaptor;
    private KafkaOutboundInterceptor kafkaOutboundInterceptor;

    public PostMessageTopicBService(OutboundAdaptor outboundAdaptor, KafkaOutboundInterceptor kafkaOutboundInterceptor) {
        this.outboundAdaptor = outboundAdaptor;
        this.kafkaOutboundInterceptor = kafkaOutboundInterceptor;
    }

    @Override
    public SystemResponseEntity execute(SystemRequestEntity systemRequestEntity) {
        outboundAdaptor.writeMessageTopicB(systemRequestEntity);
        kafkaOutboundInterceptor.intercept(topic, systemRequestEntity);

        SystemResponseEntity systemResponseEntity = new SystemResponseEntity();
        systemResponseEntity.setDateReg(systemRequestEntity.getDateReg());
        systemResponseEntity.setTimeReg(systemRequestEntity.getTimeReg());
        systemRequestEntity.setMessage(systemRequestEntity.getMessage());

        return systemResponseEntity;
    }
}
