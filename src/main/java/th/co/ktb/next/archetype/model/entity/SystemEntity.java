package th.co.ktb.next.archetype.model.entity;

import lombok.*;

/*
* This is the Entity class that used with Spring JPA to access database.
*/
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SystemEntity {
    private String dateReg;
    private String timeReg;
    private String message;
}
