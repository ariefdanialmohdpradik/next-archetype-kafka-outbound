package th.co.ktb.next.archetype.adaptor;

import th.co.ktb.next.archetype.model.request.SystemRequestEntity;

/*
* This is the interface to perform outbound API call.
* This interface contains the methods signature of the function to make outbound API call.
* */
public interface OutboundAdaptor {
    void writeMessageTopicA(SystemRequestEntity systemRequestEntity);
    void writeMessageTopicB(SystemRequestEntity systemRequestEntity);
}
