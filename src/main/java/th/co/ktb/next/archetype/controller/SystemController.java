package th.co.ktb.next.archetype.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.ktb.next.archetype.model.request.SystemRequestEntity;
import th.co.ktb.next.archetype.model.response.SystemResponseEntity;
import th.co.ktb.next.archetype.service.PostMessageTopicAService;
import th.co.ktb.next.archetype.service.PostMessageTopicBService;

/*
 * This is the controller layer of the Micro-service.
 * Description:
 * 1. The controller layer will handle those incoming APIs and pass to its respective service.
 * 2. Each controller will be categorized based on a particular API group.
 */
@RestController
@RequestMapping("/api/v1")
public class SystemController {

    private PostMessageTopicAService postMessageTopicAService;
    private PostMessageTopicBService postMessageTopicBService;

    public SystemController(PostMessageTopicAService postMessageTopicAService, PostMessageTopicBService postMessageTopicBService) {
        this.postMessageTopicAService = postMessageTopicAService;
        this.postMessageTopicBService = postMessageTopicBService;
    }

    @PostMapping("/topic-A")
    public SystemResponseEntity topicA(@RequestBody SystemRequestEntity systemRequestEntity) {
        return postMessageTopicAService.execute(systemRequestEntity);
    }

    @PostMapping("/topic-B")
    public SystemResponseEntity topicB(@RequestBody SystemRequestEntity systemRequestEntity) {
        return postMessageTopicBService.execute(systemRequestEntity);
    }
}
