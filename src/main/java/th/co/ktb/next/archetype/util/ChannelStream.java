package th.co.ktb.next.archetype.util;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Kafka Stream
 *
 * Inbound = to read message from kafka topic
 * Outbound = to write message to kafka topic
 *
 * Creating interface that defines method for each stream
 *
 * If you need custom interface, you do it here, else use Sink, Source or Processor built by SCS
 */

public interface ChannelStream {

    String OUTPUT = "output-stream";
    String OUTPUT_2 = "output-stream-2";

    @Output(OUTPUT)
    MessageChannel writeTopicA();

    @Output(OUTPUT_2)
    MessageChannel writeTopicB();
}
