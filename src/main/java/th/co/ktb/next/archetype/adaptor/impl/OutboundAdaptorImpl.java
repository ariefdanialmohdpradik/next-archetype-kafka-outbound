package th.co.ktb.next.archetype.adaptor.impl;

import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;
import th.co.ktb.next.archetype.adaptor.OutboundAdaptor;
import th.co.ktb.next.archetype.model.request.SystemRequestEntity;
import th.co.ktb.next.archetype.util.ChannelStream;

/*
* This is the implementation class that implement Adaptor Interface.
* This layer contains any logic required to make API calls to other Micro-services and backend API.
* * */

@Component
public class OutboundAdaptorImpl implements OutboundAdaptor {

    private ChannelStream channelStream;

    public OutboundAdaptorImpl(ChannelStream channelStream) {
        this.channelStream = channelStream;
    }

    @Override
    public void writeMessageTopicA(SystemRequestEntity systemRequestEntity) {
        MessageChannel messageChannel = channelStream.writeTopicA();
        messageChannel.send(MessageBuilder
                .withPayload(systemRequestEntity)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }

    @Override
    public void writeMessageTopicB(SystemRequestEntity systemRequestEntity) {
        MessageChannel messageChannel = channelStream.writeTopicB();
        messageChannel.send(MessageBuilder
                .withPayload(systemRequestEntity)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }
}
